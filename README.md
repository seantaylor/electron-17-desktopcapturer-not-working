# Electron 17 App

desktopCapturer not working on ubuntu 22 and Fedora 34. But the same code works for older ubuntu version.
Captured Screenshots will be stored on "screenshotElectron" folder inside home directory. The size of captured Screenshot will be 0 bytes on ubuntu 22 and fedora 34.


## Requirements 

Please install following packages manually.

* Node v16.13.1
* npm v8.1.2
* Electron v17.1.2

## Setup App Instructions

Clone the repository and run the below command
```
npm install
```

## Running app in development mode

```
npm run start
```

## Create debian build

```
npm run build-linux
npm run deb64
```



