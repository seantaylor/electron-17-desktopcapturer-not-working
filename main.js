// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain, desktopCapturer, screen} = require('electron')
const path = require('path')
const fs = require('fs')
var homePath = app.getPath('home');
var appDataPath = path.join(homePath,'./screenshotElectron/');
// Create folder for screenshot
if (!fs.existsSync(appDataPath)){
  console.log('folder created');
  fs.mkdirSync(appDataPath);
}

app.commandLine.appendSwitch('enable-features', 'WebRTCPipeWireCapturer');

function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      backgroundThrottling: false
    },
    webSecurity: false
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')
  mainWindow.setResizable(false)
  mainWindow.setMenuBarVisibility(false)

  // Screenshot code
  ipcMain.on('set-title', (event, title) => {
    var mainScreenSize = screen.getPrimaryDisplay();
    desktopCapturer.getSources({ types: ['screen'], thumbnailSize: mainScreenSize['size'] }).then( sources => {
      fs.writeFile(appDataPath+new Date().getTime()+`.jpg`, sources[0].thumbnail.toJPEG(25), (err) => {
        if (err) throw err
          console.log('Image Saved')
      })
    })
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
